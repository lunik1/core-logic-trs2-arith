(ns core
  (:refer-clojure :exclude [==])
  (:require [clojure.core.logic :refer :all]))

;;; Implementation of the arithmetic system used in 'The Reasoned
;;; Schemer, Second Edition,' by Friedman, Byrd, Kiselyov, and Hemann
;;; (MIT Press, 2018) for Clojure's core.logic

;;; Definitions are presented in the order in which they appear in
;;; Chapters 7 and 8

;;; Definitions which already have an equivalent in core.logic are not
;;; included.

;; Frame 7:5
(defne bit-xoro
  "A relation where x, y, and r are bits, such that x XOR y = r"
  [x y r]
  ([0 0 0])
  ([0 1 1])
  ([1 0 1])
  ([1 1 1]))

;; Frame 7:5
(defne bit-nando
  "A relation where x, y, and r are bits, such that x NAND y = r"
  [x y r]
  ([0 0 1])
  ([0 1 1])
  ([1 0 1])
  ([1 1 0]))

;; Frame 7:5, alternative definition of bit-xoro
(comment
  (defn bit-xoro
    "A relation where x, y, and r are bits, such that x XOR y = r"
    [x y r]
    (fresh [s t u]
           (bit-nando x y s)
           (bit-nando s y u)
           (bit-nando x s t)
           (bit-nando t u r))))

;; Frame 7:10
(defne bit-ando
  "A relation where x, y, and r are bits, such that x AND y = r"
  [x y r]
  ([0 0 0])
  ([1 0 0])
  ([0 1 0])
  ([1 1 1]))

;; Frame 7:10
(defn bit-noto
  "A relation where x and ar are bits, such that r = NOT x"
  [x r]
  (bit-nando x x r))

;; Frame 7:10, alternative definition of bit-ando
(comment
  (defn bit-ando [x y r]
    (fresh [s]
           (bit-nando x y s)
           (bit-noto s r))))

;; Frame 7:12
(defn half-addero
  "A relation where x, y, r, and c are bits satisfying x + y = r + 2c"
  [x y r c]
  (and*
   [(bit-xoro x y r)
    (bit-ando x y c)]))

;; Frame 7:12, alternative definition of half-addero
(comment
  (defne half-addero
    "A relation where x, y, r, and c are bits satisfying x + y = r + 2c"
    [x y r c]
    ([0 0 0 0])
    ([1 0 1 0])
    ([0 1 1 0])
    ([1 1 0 1])))

;; Frame 7:15
(defn full-addero
  "A relation in which the bits b, x, y, r, and c satisfy b + x + y = r + 2c"
  [b x y r c]
  (fresh [w xy wz]
         (half-addero x y w xy)
         (half-addero w b r wz)
         (bit-xoro xy wz c)))

;; Frame 7:15, alternative definition of full-addero
(comment
  (defne full-addero
    "A relation in which the bits b, x, y, r, and c satisfy b + x + y = r + 2c"
    [b x y r c]
    ([0 0 0 0 0])
    ([1 0 0 1 0])
    ([0 1 0 1 0])
    ([1 1 0 0 1])
    ([0 0 1 1 0])
    ([1 0 1 0 1])
    ([0 1 1 0 1])
    ([1 1 1 1 1])))

;; Frame 7:42
;; Unlike code in TRS, asserts n is a natural number
(defn build-num
  "Takes n ∈ ℕ₀ and returns n's representation in little-endian binary as a list
  of 0s and 1s.

  Functions and goals in this module assume numbers are provided in this format."
  [n]
  {:pre [(nat-int? n)]}
  (cond
    (zero? n) ()
    (even? n) (cons 0 (build-num (/ n 2)))
    (odd? n) (cons 1 (build-num (/ (- n 1) 2)))))

;; Frame 7:77
(defn poso
  "A goal that succeeds iff n is positive (i.e. n ∈ ℕ₁)."
  [n]
  (== (lcons (lvar) (lvar)) n))

;; Frame >1o
(defn >1o
  "A goal that succeeds iff n is >1."
  [n]
  (== (llist (lvar) (lvar) (lvar)) n))

(declare gen-addero)

;; Frame 7:104
(defne addero
  "A relation where b + n + m = r. b is a carry bit and n, m, r are positive
  integers."
  [b n m r]
  ([0 x () x])
  ([0 () x x]
   (poso m))
  ([1 _ () _]
   (addero 0 n '(1) r))
  ([1 () _ _]
   (addero 0 '(1) m r))
  ([_ '(1) '(1) _]
   (fresh [a c]
          (== (list a c) r)
          (full-addero b 1 1 a c)))
  ([_ '(1) _ _]
   (gen-addero b n m r))
  ([_ _ '(1) _]
   (>1o n)
   (>1o r)
   (addero b '(1) n r))
  ([_ _ _ _]
   (>1o n)
   (gen-addero b n m r)))

;; Frame 7:104
(defne gen-addero
  "A relation where b + n + m = r. b is a carry bit; n is positive; and m,
  r are >1."
  [b n m r]
  ([_ [a . x] [d . y] [c . z]]
   (fresh [e]
          (poso y)
          (poso z)
          (full-addero b a d c e)
          (addero e x y z))))

;; Frame 7:114
(defn +o
  "A relation where n + m = k. n, m, k ∈ ℕ₀."
  [n m k]
  (addero 0 n m k))

;; Frame 7:116
(defn -o
  "A relation where n - m = k. n, m, k ∈ ℕ₀."
  [n m k]
  (+o m k n))

;; Frame 7:120
(defne lengtho
  "A relation where l is the length of list n."
  [l n]
  ([() ()])
  ([[_ . d] _]
   (fresh [res]
          (+o '(1) res n)
          (lengtho d res))))

(declare odd-*o)

;; Frame 8:9
(defne *o
  "A relation where n * m = k. n, m, k ∈ ℕ₀."
  [n m p]
  ([() _ ()])
  ([_ () ()]
   (poso n))
  (['(1) x x]
   (poso x))
  ([x '(1) x]
   (>1o x))
  ([[0 . x] _ [0 . z]]
   (poso x)
   (poso z)
   (>1o m)
   (*o x m z))
  ([[1 . x] [0 . y] _]
   (poso x)
   (poso y)
   (*o m n p))
  ([[1 . x] [1 . y] _]
   (poso x)
   (poso y)
   (odd-*o x n m p)))

(declare bound-*o)

;; Frame 8:18
(defn odd-*o
  [x n m p]
  (fresh [q]
         (bound-*o q p n m)
         (*o x m q)
         (+o (lcons 0 q) m p)))

;; Frame 8:24
(defne bound-*o
  [q p n m]
  ([() _ _ _]
   (poso p))
  ([[_ . x] [_ . y] _ _]
   (matche [n m]
           ([() [_ . z]]
            (bound-*o x y z ()))
           ([[_ . z] _]
            (bound-*o x y z m)))))

;; Frame 8:28
(defne =lo
  "A relation where the bit representations of n and m are the same length. n, m
  ∈ ℕ₀."
  [n m]
  ([() ()])
  (['(1) '(1)])
  ([[_ . x] [_ . y]]
   (poso x)
   (poso y)
   (=lo x y)))

;; Frame 8:36
(defne <lo
  "A relation where the bit representation of n is shorter than m. n, m ∈ ℕ₀."
  [n m]
  ([() _]
   (poso m))
  (['(1) _]
   (>1o m))
  ([[_ . x] [_ . y]]
   (poso x)
   (poso y)
   (<lo x y)))

;; Frame 8:40
(defn <=lo
  "A relation where the bit representation of n is shorter than or the same
  length as m. n, m ∈ ℕ₀."
  [n m]
  (or*
   [(=lo n m)
    (<lo n m)]))

;; Frame 8:46
(defn <o
  "A relation where n < m. n, m ∈ ℕ₀."
  [n m]
  (conde
   [(<lo n m)]
   [(=lo n m)
    (fresh [x]
           (poso x)
           (+o n x m))]))

;; Frame 8:46
(defn <=o
  "A relation where n ≤ m. n, m ∈ ℕ₀."
  [n m]
  (or*
   [(== n m)
    (<o n m)]))

;; Frame 8:54, first (flawed) definition of divo
(comment
  (defne divo
    "A relation where n / m = q with remainder r. n, m, q, r ∈ ℕ₀."
    [n m q r]
    ([x _ () x]
     (<o n m))
    ([x x '(1) ()]
     (<o r m))
    ([_ _ _ _]
     (<o m n)
     (<o r m)
     (fresh [mq]
            (<=lo mq n)
            (*o m q mq)
            (+o mq r n)))))

;; Frame 8:64, second (flawed) definition of divo
(comment
  (defn divo
    "A relation where n / m = q with remainder r. n, m, q, r ∈ ℕ₀."
    [n m q r]
    (fresh [mq]
           (<o r m)
           (<=lo mq n)
           (*o m q mq)
           (+o mq r n))))

;; Frame 8:69
(defne splito
  [n r l h]
  ([() _ () ()])
  ([[0 b . m] () () [b . m]])
  ([[1 . m] () '(1) m])
  ([[0 b . m] [_ . s] () _]
   (splito (lcons b m) s () h))
  ([[1 . m] [_ . s] '(1) _]
   (splito m s () h))
  ([[b . m] [_ . s] [b . ll] _]
   (poso ll)
   (splito m s ll h)))

(declare n-wider-than-mo)

;; Frame 8:81, final definition of divo
(defne divo
  "A relation where n / m = q with remainder r. n, m, q, r ∈ ℕ₀."
  [n m q r]
  ([x _ () x]
   (<o n m))
  ([_ _ '(1) _]
   (=lo m n)
   (+o r m n)
   (<o r m))
  ([_ _ _ _]
   (poso q)
   (<lo m n)
   (<o r m)
   (n-wider-than-mo n m q r)))

;; Frame 8:81
(defn n-wider-than-mo
  [n m q r]
  (fresh [nhigh nlow qhigh qlow mqlow mrqlow rr rhigh]
         (splito n r nlow nhigh)
         (splito q r qlow qhigh)
         (matche [nhigh qhigh]
                 ([() ()]
                  (-o nlow r mqlow)
                  (*o m qlow mqlow))
                 ([_ _]
                  (poso nhigh)
                  (*o m qlow mqlow)
                  (+o r mqlow mrqlow)
                  (-o mrqlow nlow rr)
                  (splito rr r () rhigh)
                  (divo nhigh m qhigh rhigh)))))

(declare exp2o base-three-or-moreo)

;; Frame 8:84
(defne logo
  "A relation where n = b^q + r. n, b, r, q ∈ ℕ₀ and q is the largest number
  that satisfies the relation."
  [n b q r]
  ([_ _ () _]
   (<=o n b)
   (+o r '(1) n))
  ([_ _ '(1) _]
   (>1o b)
   (=lo n b)
   (+o r b n))
  ([_ '(1) _ _]
   (poso q)
   (+o r '(1) n))
  ([x () _ x]
   (poso q))
  ([[_ _ . dd] '(0 1) _ _]
   (poso dd)
   (exp2o n () q)
   (splito n dd r (lvar)))
  ([_ _ _ _]
   (<=o '(1 1) b)
   (<lo b n)
   (base-three-or-moreo n b q r)))

;; Frame 8:84
(defne exp2o
  [n b q]
  (['(1) _ ()])
  ([_ _ '(1)]
   (>1o n)
   (splito n b (lvar) '(1)))
  ([_ _ [0 . q1]]
   (fresh [b2]
          (poso q1)
          (<lo b n)
          (appendo b (lcons 1 b) b2)
          (exp2o n b2 q1)))
  ([_ _ [1 . q1]]
   (fresh [nhigh b2]
          (poso q1)
          (poso nhigh)
          (splito n b (lvar) nhigh)
          (appendo b (lcons 1 b) b2)
          (exp2o nhigh b2 q1))))

(declare repeated-mulo)

;; Frame 8:84
(defn base-three-or-moreo
  [n b q r]
  (fresh [bw1 bw nw nw1 qlow1 qlow s]
         (exp2o b () bw1)
         (+o bw1 '(1) bw)
         (<lo q n)
         (fresh [q1 bwq1]
                (+o q '(1) q1)
                (*o bw q1 bwq1)
                (<o nw1 bwq1))
         (exp2o n () nw1)
         (+o nw1 '(1) nw)
         (divo nw bw qlow1 s)
         (+o qlow '(1) qlow1)
         (<=lo qlow q)
         (fresh [bqlow qhigh qdhigh qd]
                (repeated-mulo b qlow bqlow)
                (divo nw bw1 qhigh (lvar))
                (+o qlow qdhigh qhigh)
                (+o qlow qd q)
                (<=o qd qdhigh)
                (fresh [bqd bq1 bq]
                       (repeated-mulo b qd bqd)
                       (*o bqlow bqd bq)
                       (*o b bq bq1)
                       (+o bq r n)
                       (<o n bq1)))))

;; Frame 8:84
(defne repeated-mulo
  [n q nq]
  ([_ () '(1)]
   (poso n))
  ([x '(1) x])
  ([_ _ _]
   (>1o q)
   (fresh [q1 nq1]
          (+o q1 '(1) q)
          (repeated-mulo n q1 nq1)
          (*o nq1 n nq))))

;; Frame 8:93
(defn expo
  "A relation where n = b^q. n, b, q, ∈ ℕ₀."
  [b q n]
  (logo n b q ()))
